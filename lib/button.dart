import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import './button.dart';

class Button extends StatelessWidget {
  const Button({Key? key, required this.icon, required this.label})
      : super(key: key);

  final IconData icon;
  final String label;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      icon: Icon(
        icon,
        color: Colors.white,
        size: 20,
      ),
      onPressed: () {
        _rating(context);
      },
      label: Text(
        label.toString(),
        style: const TextStyle(fontSize: 12),
      ),
      style: ElevatedButton.styleFrom(minimumSize: Size(100, 50)),
    );
  }
}

void _rating(BuildContext context) {
  showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Trage hier deine Bewertung ein - Danke!'),
            RatingBar.builder(
              minRating: 1,
              itemBuilder: (context, _) => const Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {},
            ),
            SizedBox(height: 20),
            Button(icon: Icons.save, label: 'SPEICHERN'),
          ],
        );
      });
}
