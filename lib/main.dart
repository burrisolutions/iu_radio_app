import 'package:flutter/material.dart';
import './song.dart';
import './button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Tune On Radio'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Song(),
          Container(
            child: const Text(
              'Weitere Songs in dieser Playlist',
              style: TextStyle(fontSize: 18, decoration: TextDecoration.underline),
            ),
            margin: const EdgeInsets.all(10),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text('\u2022 Bohemian Rhapsody / Queen',),
                Text('\u2022 Song / Elton John',),
                Text('\u2022 Lonely / Justin Bieber'),
                Text('\u2022 Shivers / Ed Sheeran'),
                Text('\u2022 Cold Heart - PNAU Remmix / Elton John, Dua Lipa, PNAU'),
                Text('\u2022 Attention / Omah Lay, Justin Bieber'),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const <Widget>[
              Button(icon: Icons.star, label: 'BEWERTUNG\nPLAYLIST'),
              Button(icon: Icons.star, label: 'BEWERTUNG\nMODERATOR'),
              Button(icon: Icons.star, label: 'MODERATOREN-\nBEWERTUNG'),
            ],
          ),
        ],
      ),
    );
  }
}
