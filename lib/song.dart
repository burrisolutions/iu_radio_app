import 'package:flutter/material.dart';

class Song extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        alignment: Alignment.center,
        child: Column(children: <Widget>[
          const SizedBox(height: 30),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Aktuell gespielter Song:',
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
            child: Image.asset(
              'assets/images/song.jpg',
            ),
          ),
          const Text(
            "Michael Jackson",
            style: TextStyle(
              fontSize: 30,
            ),
          ),
          const SizedBox(height: 5),
          const Text(
            "Billie Jean / Thriller",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          const SizedBox(height: 30),
        ]));
  }
}
